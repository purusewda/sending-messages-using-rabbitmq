const express = require("express");
const app = express();
const amqp = require('amqplib');
let channel;

connect();
async function connect() {
	try {
		const amqpServer = "amqp://localhost:5672";
		connection = await amqp.connect(amqpServer);
		channel = await connection.createChannel();
		await channel.assertQueue("rabbit");
        channel.consume("rabbit", data => {
            console.log(`Received data at 5002: ${Buffer.from(data.content)}`);
            channel.ack(data);
        });
	} catch (error) {
		console.log(error);
	}
}

app.listen(5002, () => {
	console.log("Server at 5002");
});

